from flask import Flask
import os
import pymysql.cursors

app = Flask(__name__)

connection = pymysql.connect(host=os.environ['MYSQL_HOST'],
                             user=os.environ['MYSQL_USER'],
                             password=os.environ['MYSQL_PASSWORD'],
                             db=os.environ['MYSQL_DB'],
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
app = Flask(__name__)

@app.route("/")
def hello():
    with connection.cursor() as cursor:
        # Create a new record
        sql = "INSERT INTO counter () VALUE ()"
        cursor.execute(sql)

    # connection is not autocommit by default. So you must commit to save
    # your changes.
    connection.commit()

    with connection.cursor() as cursor:
        # Read a single record
        cursor.execute("SELECT * FROM counter ORDER BY id DESC LIMIT 1")
        result = cursor.fetchone()
        print(result)
        return "Counter Value From DB: {}".format(result['id'])
    return "Hello form Flask"

if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)