from flask import Flask
import pymysql.cursors

# Connect to the database
connection = pymysql.connect(host='mysql',
                             user='root',
                             password='IrepIrep',
                             db='test',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

try:
    with connection.cursor() as cursor:
        # Create a new record
        sql = "INSERT INTO counter () VALUE ()"
        cursor.execute(sql)

    # connection is not autocommit by default. So you must commit to save
    # your changes.
    connection.commit()

    with connection.cursor() as cursor:
        # Read a single record
        cursor.execute("SELECT * FROM counter")
        result = cursor.fetchall()
        print(result)
finally:
    connection.close()